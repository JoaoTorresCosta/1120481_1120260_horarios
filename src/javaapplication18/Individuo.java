/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Comparator;
import utilitarios.*;
/**
 *
 * @author Pedro
 */
public abstract class Individuo  { //coloda a abstrat para nao ser instanciavel
    private String SiglaOuNumero;  //Esta variavel SiglaOuNumero esta a substituir a SiglaOuNumero no caso do professor e o numero mecanografico no caso do aluno
    private String nome;   //pode ser admitido como campo da designação?
    private String mail;
    private Data data;

    public Individuo() {
        this.SiglaOuNumero="Sigla inválida.";
        this.nome="Nome inválido.";
        this.mail="Mail invalido.";
        this.data=new Data ();
    }
    
    
    public Individuo(String sigla, String nome, String mail,Data data) {
        setSiglaOuNumero(sigla);
        setNome(nome);
        setMail(mail);
        this.data=data;
    }

    public Data getData() {
        return data;
    }

    //construtor copia da classe individuo.
    public Individuo(Individuo p){
    SiglaOuNumero=p.getSiglaOuNumero();
    nome=p.getNome();
    mail=p.getMail();
    data=p.getData();
    }
    public String getSiglaOuNumero() {
        return SiglaOuNumero;
    }

    private void setSiglaOuNumero(String sigla) {
        this.SiglaOuNumero = sigla;
    }

    public String getNome() {
        return nome;
    }

    private void setNome(String nome) {
        this.nome = nome;
    }

    public String getMail() {
        return mail;
    }

    private void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Individuo{" + "sigla=" + SiglaOuNumero + ", nome=" + nome + ", mail=" + mail + ", data=" + data.getDia() + data.getMes() + data.getAno()+'}';
    }
    
   
}
