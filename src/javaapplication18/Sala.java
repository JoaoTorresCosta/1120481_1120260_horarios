/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;

import utilitarios.*;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Sala {
   private String codigo;
   private String tipoSala;
   private int capacidade;

    public Sala(String codigo, String tipoSala, int capacidade) {
     setCodigo(codigo);
     setTipoSala(tipoSala);
     setCapacidade(capacidade);
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    private void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the tipoSala
     */
    public String getTipoSala() {
        return tipoSala;
    }

    /**
     * @param tipoSala the tipoSala to set
     */
    private void setTipoSala(String tipoSala) {
        this.tipoSala = tipoSala;
    }

    /**
     * @return the capacidade
     */
    public int getCapacidade() {
        return capacidade;
    }

    /**
     * @param capacidade the capacidade to set
     */
    private void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    @Override
    public String toString() {
        return "Sala{" + "codigo=" + codigo + ", tipoSala=" + tipoSala + ", capacidade=" + capacidade + '}';
    }
   
    public static void listarDadosSala(String codigo,ArrayList<Sala> salas){
       Sala salaAlistar;
       for (Sala sala : salas) {
           if(sala.getCodigo().equalsIgnoreCase(codigo)){salaAlistar=(Sala)sala; 
           System.out.println(salaAlistar);}
       }
      
   }


public static void listarSalaPorPercentagemDeOcupacao(double percentagem,ArrayList <Horario> horarios,ArrayList<Sala>salas){
double somaDuracaoAula;
int validarSeExistePeloMenusUma=0;
for (Sala sala : salas) {
    somaDuracaoAula=0;
    for (Horario horario2 : horarios) {
         if(horario2.getSala()==sala)
         {  
         somaDuracaoAula+=horario2.getDuracaoDaAula();
         }
         
     }
      if(somaDuracaoAula>(percentagem/100)*50)
      {   validarSeExistePeloMenusUma++;
          System.out.println("Esta sala "+sala.codigo+ " tem uma ocupaçao maior do que esta percentagem:"+percentagem);
      }
      
    }
if (validarSeExistePeloMenusUma==0){
    System.out.println("Nao existem salas com esta percentagem:"+percentagem+" de ocupação");
}

}



}