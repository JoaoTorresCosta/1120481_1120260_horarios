
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Aluno extends Individuo  { 
   
    private String contactoTelefonico;
     
    public Aluno(String numero, String nome, Data data, String contactoTelefonico,String mail) {
        super(numero,nome,mail,data);
        setContactoTelefonico(contactoTelefonico);
    }

    public String getContactoTelefonico() {
        return contactoTelefonico;
    }

    private void setContactoTelefonico(String contactoTelefonico) {
        this.contactoTelefonico = contactoTelefonico;
    }

    @Override
    public String toString() {
        return "Aluno{" + "numero=" + getSiglaOuNumero() + ", nome=" + getNome() + ", mail=" + getMail() + ", data=" + getData()+", Contacto telefonico="+getContactoTelefonico()+'}';
    }
  
   public static void listarDadosAlunoNumero(String numeroAluno,ArrayList<Individuo> alunos){
       Aluno alunoAlistar;
       for (Individuo aluno : alunos) {
           if(aluno.getSiglaOuNumero().equalsIgnoreCase(numeroAluno)){alunoAlistar=(Aluno)aluno;
           System.out.println(alunoAlistar);}
       }
       
   }
   public static void listarDadosAlunoNome(String nomeAluno,ArrayList<Individuo> alunos){
       Aluno alunoAlistar;
       for (Individuo aluno : alunos) {
           if(aluno.getNome().equalsIgnoreCase(nomeAluno)){alunoAlistar=(Aluno)aluno;
           System.out.println(alunoAlistar);}
       }
       
   }

}
