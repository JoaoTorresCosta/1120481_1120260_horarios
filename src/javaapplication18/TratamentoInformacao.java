/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class TratamentoInformacao {
   
    
    public static int contarLinhas(String nomeFicheiro) throws Exception {
       
    int numeroLinhas = 0;                       //variável que guarda o nº de linhas;
       
        Scanner abrirFicheiro = new Scanner(new File(nomeFicheiro));  //abre o ficheiro;

        while (abrirFicheiro.hasNextLine()) {     //verifica se tem próxima linha;
            abrirFicheiro.nextLine();             //troca de linha;
            numeroLinhas++;
        }
        abrirFicheiro.close();                    //fecha o ficheiro;
        return numeroLinhas;                      //retorno do número de linhas;
    }
    
    public static void trataInformacao(String nomeFicheiro, int numeroLinhas, String vecInformação[]) throws Exception { //aloca a informaçao de um ficheiro num vector;


        Scanner abrirFicheiro = new Scanner(new File(nomeFicheiro));
        abrirFicheiro.nextLine(); //passa a linha da estrutura para nao a intruduzir no array pois nao tem dados 
        
        for (int i = 0; i < vecInformação.length; i++) {
            vecInformação[i] = abrirFicheiro.nextLine();

        }
        abrirFicheiro.close();
    }
    
    public static void trataInformacaoAlunos(ArrayList <Individuo> alunos, String vecInformação []) throws Exception {
      
      String vecaux [] = vecInformação[0].split(";");
      String vecauxData[]= vecaux[2].split("/");
      int vecauxData2[]=new int[3];
      
      
      for (int i = 0; i < vecInformação.length ; i++) {
        vecaux=vecInformação[i].split(";");
        vecauxData=vecaux[2].split("/");
        vecauxData2[0]=Integer.parseInt(vecauxData[0]);
        vecauxData2[1]=Integer.parseInt(vecauxData[1]);
        vecauxData2[2]=Integer.parseInt(vecauxData[2]);
        
    alunos.add(new Aluno(vecaux[0],vecaux[1],new Data(vecauxData2[2],vecauxData2[1],vecauxData2[0]),vecaux[3],vecaux[4]));
      }
        
    }
    
    public static void trataInformacaoProfessores(ArrayList <Individuo> professores, String vecInformação []) throws Exception {
      String vecaux [] = vecInformação[0].split(";");
      String vecauxData[]= vecaux[2].split("/");
      int vecauxData2[]=new int[3];
      
      
      for (int i = 0; i < vecInformação.length ; i++) {
        vecaux=vecInformação[i].split(";");
        vecauxData=vecaux[2].split("/");
        vecauxData2[0]=Integer.parseInt(vecauxData[0]);
        vecauxData2[1]=Integer.parseInt(vecauxData[1]);
        vecauxData2[2]=Integer.parseInt(vecauxData[2]);
        
    professores.add(new Professor(vecaux[0],vecaux[1],new Data(vecauxData2[2],vecauxData2[1],vecauxData2[0]),vecaux[3]));
      }
        
   }
    
    public static void trataInfomacaoDisciplinas(ArrayList <Disciplina> disciplinas,String vecInformação [],ArrayList <Individuo> professores){
        String vecaux [] = vecInformação[0].split(";");
        int vecauxInteiros[]=new int[2];
        Professor professorT=new Professor(null, null, null, null);//so para inicializar senao queixa-se que nao esta inicializado
        
        for (int i = 0; i < vecInformação.length; i++) {
           vecaux=vecInformação[i].split(";");
           vecauxInteiros[0]=Integer.parseInt(vecaux[2]);
           vecauxInteiros[1]=Integer.parseInt(vecaux[3]);
           ArrayList <Individuo> professoresP =new ArrayList<Individuo>();
           
           for (Individuo professor : professores) 
           {
              if (professor.getSiglaOuNumero().equals(vecaux[4])) 
                {                                           //nesta linha ele procura no arraylist o se existe o   
                 professorT=(Professor)professor;        //professor e entao copia-o para poder ser adcionado 
                }                                    //aos parametros para criar uma disciplina
              
           }
          
            for (Individuo professor : professores) 
            {       
                for (int j = 5; j < vecaux.length; j++) 
                {
                  if (professor.getSiglaOuNumero().equals(vecaux[j]))  //nesta linha ele procura no arraylist o se existe o   
                                                             //professor e entao copia-o para poder ser adcionado 
                    {                                   //aos parametros para criar uma disciplina, a diferença
                     professoresP.add(professor);     //do de cima é que aqui procura todos os n professores
                    }                               //que lecional aulas praticas dai ser preciso outro for
                    
                }
     
            }
           
     disciplinas.add(new Disciplina(vecaux[0],vecaux[1],vecauxInteiros[0],vecauxInteiros[1],professorT,professoresP));    
        }
  
        
    }
    
    
    
    public static void trataInfomacaoTurma (String vecInformação [],ArrayList<Individuo> alunos,ArrayList<Turma> turmas){
        String vecaux [] = vecInformação[0].split(";");
        
           for (int i = 0; i < vecInformação.length; i++) {
           vecaux=vecInformação[i].split(";");
           ArrayList <Individuo> alunosTurma =new ArrayList<Individuo>();
               for (Individuo aluno : alunos) {
                   
                   for (int j = 1; j < vecaux.length; j++) 
                {
                  if (aluno.getSiglaOuNumero().equals(vecaux[j]))   
                                                           
                    {                           
                     alunosTurma.add(aluno);    
                    }                              
               }
           
           
           }
           turmas.add(new Turma(vecaux[0],alunosTurma));
        }
    }
    
    
    public static void trataInfomacaoSala (String vecInformacao[],ArrayList<Sala>salas)
    {
        String vecaux [] = vecInformacao[0].split(";");
        int capacidade=0;
        for (int i = 0; i < vecInformacao.length; i++) {
             vecaux=vecInformacao[i].split(";");
             capacidade=Integer.parseInt(vecaux[2]);
             salas.add(new Sala(vecaux[0],vecaux[1],capacidade));
            
        }
    }
    
    
    
    public static void trataInfomacaoHorario (String vecInformacao [],ArrayList<Individuo> professores,ArrayList<Horario> horarios,ArrayList<Turma> turmas, ArrayList<Disciplina> disciplinas, ArrayList<Sala> salas ){
    String vecaux [] = vecInformacao[0].split(";");
    String tipoAula="";
        for (int i = 0; i < vecInformacao.length; i++) 
        {
            vecaux=vecInformacao[i].split(";");
            Turma turmasHorario =new Turma(null,null);
            Disciplina disciplinaHorario =new Disciplina(null,null,0,0,null,null);
            Professor professorHorario =new Professor(null,null,null,null);
            Sala salaHorario =new Sala(null,null,0);
            int duracaoAula=0;
            int diaSemanahorario=Integer.parseInt(vecaux[3]);
            int horaIniciohorario=Integer.parseInt(vecaux[4]);
            for (Turma turma : turmas) 
            {
                    if(turma.getCodigoTurma().equals(vecaux[0]))
                    {
                    turmasHorario=turma;
                    }
            }
            for (Disciplina disciplina : disciplinas) 
            {
                if (disciplina.getSigla().equals(vecaux[1])) 
                {
                disciplinaHorario=disciplina;    
                }
            }
             for (Individuo professor : professores) 
            {       
                  if (professor.getSiglaOuNumero().equals(vecaux[6]))  
                    {          
                     professorHorario=(Professor)professor;
                    }                              
                    
             }
             for (Sala sala : salas) 
            {       
                  if (sala.getCodigo().equals(vecaux[7]))  
                    {          
                     salaHorario=sala;
                     if(sala.getTipoSala().equalsIgnoreCase("anfiteatro")){duracaoAula=1;tipoAula="Teorica";}
                     else{duracaoAula=2;tipoAula="Pratica";}
                    }                              
                    
             }
     horarios.add(new Horario(turmasHorario,disciplinaHorario,tipoAula,diaSemanahorario,horaIniciohorario,duracaoAula,professorHorario,salaHorario));
           }
                
         }
            
        }
    
    
    
