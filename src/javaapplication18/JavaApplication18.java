/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import sun.text.resources.CollationData;
/**
 *
 * @author Pedro
 */
public class JavaApplication18 {

     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
     //variaveis necessarias para o main;
     String nomeFicheiro="teste.txt";
     int numeroLinhas;
     
     //processo contar linhas dos documentos e criar arrays com informaçao dos ficheiros:
     numeroLinhas=TratamentoInformacao.contarLinhas("alunos.txt");    //alunos
     String vecInformacaoAlunos [] = new String [numeroLinhas-1];     // o menus 1 é por causa da linha 1 do ficheiro ser a estrutura dos dados e nao os dados em si
     TratamentoInformacao.trataInformacao("alunos.txt", numeroLinhas, vecInformacaoAlunos); //Funcional(adiciona num vector String a informaçao de um ficheiro) 
     numeroLinhas=TratamentoInformacao.contarLinhas("professores.txt");    //professor
     String vecInformacaoProfessores [] = new String [numeroLinhas-1];   
     TratamentoInformacao.trataInformacao("professores.txt", numeroLinhas, vecInformacaoProfessores); 
     numeroLinhas=TratamentoInformacao.contarLinhas("disciplinas.txt");     //disciplina
     String vecInformacaoDisciplinas[]=new String [numeroLinhas-1];
     TratamentoInformacao.trataInformacao("disciplinas.txt", numeroLinhas, vecInformacaoDisciplinas);
     numeroLinhas=TratamentoInformacao.contarLinhas("turmas.txt");     
     String vecInformacaoTurmas[]=new String [numeroLinhas-1];
     TratamentoInformacao.trataInformacao("turmas.txt", numeroLinhas, vecInformacaoTurmas);
     numeroLinhas=TratamentoInformacao.contarLinhas("salas.txt");     
     String vecInformacaoSalas[]=new String [numeroLinhas-1];
     TratamentoInformacao.trataInformacao("salas.txt", numeroLinhas, vecInformacaoSalas);
     numeroLinhas=TratamentoInformacao.contarLinhas("horarios.txt");     
     String vecInformacaoHorarios[]=new String [numeroLinhas-1];
     TratamentoInformacao.trataInformacao("horarios.txt", numeroLinhas, vecInformacaoHorarios);
     
     //Criação de arrayslist necessários:
     ArrayList <Individuo> alunos = new ArrayList<Individuo>();   //criaçao de arraylist necessarios
     ArrayList <Individuo> professores = new ArrayList<Individuo>();
     ArrayList <Disciplina> disciplinas = new ArrayList<Disciplina>();
     ArrayList <Turma> turmas = new ArrayList<Turma>();
     ArrayList <Sala> salas = new ArrayList<Sala>();
     ArrayList <Horario> horarios = new ArrayList<Horario>();
     
   
     
     //testes às funcionalinddes:
     
     
     //teste aos alunos;

     TratamentoInformacao.trataInformacaoAlunos(alunos,vecInformacaoAlunos);
        for (int i = 0; i < alunos.size(); i++) {
            System.out.println(alunos.get(i));
        }
        
     //teste aos professores;  
     TratamentoInformacao.trataInformacaoProfessores(professores,vecInformacaoProfessores);
            for (int i = 0; i < professores.size(); i++) {
            System.out.println(professores.get(i));
            }  
    
    //teste às disciplinas;
            TratamentoInformacao.trataInfomacaoDisciplinas(disciplinas, vecInformacaoDisciplinas, professores);
            for (int i = 0; i < disciplinas.size(); i++) {
            System.out.println(disciplinas.get(i));
            }  
            
     //teste às turmas;
            TratamentoInformacao.trataInfomacaoTurma(vecInformacaoTurmas, alunos,turmas);
            for (int i = 0; i < turmas.size(); i++) {
            System.out.println(turmas.get(i));
            }  
     //teste às salas;
            TratamentoInformacao.trataInfomacaoSala(vecInformacaoSalas, salas);
            for (int i = 0; i < salas.size(); i++) {
            System.out.println(salas.get(i));
            }  
     //teste aos horarios;
            TratamentoInformacao.trataInfomacaoHorario(vecInformacaoHorarios,professores,horarios,turmas,disciplinas, salas);
            for (int i = 0; i < horarios.size(); i++) {
            System.out.println(horarios.get(i));
            }  
      //testes às ordenaçoes dos alunos das turmas;      
            Turma.ListarCrescenteNumero("1DA", turmas);
            System.out.println("\n\n");
            Turma.ListarDecrescenteNumero("1DA", turmas);
            System.out.println("\n\n");
            Turma.ListarCrescenteNome("1DA", turmas);
            System.out.println("\n\n");
            Turma.ListarDecrescenteNome("1DA", turmas);
            
      //teste dados de um determinado objecto;
            System.out.println("\n");
            Aluno.listarDadosAlunoNumero("1110623", alunos);
            System.out.println("\n");
            Aluno.listarDadosAlunoNome("PATRICIA PINHO PINTO", alunos);
            System.out.println("\n");
            Professor.listarDadosProfessorNome("RAFAEL RUI MARTINS PORTO", professores);
            System.out.println("\n");
            Professor.listarDadosProfessorSigla("RRP", professores);
            System.out.println("\n");
            Sala.listarDadosSala("B201", salas);
            System.out.println("\n");
            Turma.listarDadosTurma("1DA", turmas);
            System.out.println("\n");
            Disciplina.listarDadosDisciplinaSigla("MAT", disciplinas);
            System.out.println("\n");
            Disciplina.listarDadosDisciplinaDescricao("MATEMATICA", disciplinas);
            System.out.println("\nllaslda");
            Disciplina.listarDadosDisciplinaProfessorAturma("RRP", horarios);
            
            //testar ocupaçao da aula segundo um valor defenido pelo utilizador
            Sala.listarSalaPorPercentagemDeOcupacao(32,horarios,salas);
            
            //testar carga horaria dos objectos a tratar 
            MetodosGerais.apresentaCargaHoraria("RRP", disciplinas, horarios, turmas, salas,alunos,professores );
            System.out.println("\n");
            //testar metodo listar disciplinas de uma determinada sala
            Disciplina.listarDisciplinasNumaSala(horarios, "B301");
            System.out.println("\n");
            //testar metodo listar professores que lecionam disciplinas a uma dada turma indicando o tipo de aula;
            Professor.listarProfessoresDeUmaDadaTurma(horarios, "1DA");
            System.out.println("\n");
            //teste a verificaçao de sala para permitir uma reuniao;
            Horario.encontrarPeriodoLivre(10, 2, 8, "B305", horarios); //nao se encotra disponivel 
            System.out.println("\n");
            Horario.encontrarPeriodoLivre(2, 2, 8, "B305", horarios); //disponivel para o evento 
            System.out.println("\n");
            //teste apresentar horarios;
            Horario.apresentarHorarioIndividuo("MMP", horarios);
            System.out.println("\n");
            Horario.apresentarHorarioIndividuo("PATRICIA PINHO PINTO", horarios);
            System.out.println("\n");
            Horario.mostarHorariosSala("B305", horarios);
            System.out.println("\n");
            Horario.mostarHorariosTurma("1DA", horarios);
            System.out.println("\n");            
            Horario.mostarHorariosDisciplina("ING", horarios);
   
            System.out.println("\n");
          
            //teste à inserçao de novos horarios;
            horarios=Horario.InserirCelula(new Horario(turmas.get(0),disciplinas.get(1),"P",5,8,2,(Professor)professores.get(6),salas.get(2)), horarios);
            for (Horario horario : horarios) {
                System.out.println(horario);
               
        }
          System.out.println("\n");
          //teste a eliminaçao de celulas:
          horarios= Horario.EliminarCelula(5, 8, "1DC", horarios);
           
           for (Horario  horario : horarios) {
               System.out.println(horario);
            
           }
            System.out.println("\n");
           horarios=Horario.EditarCelula(5, 8, "1DA", salas.get(6), professores.get(6), disciplinas.get(1),  horarios);
          for (Horario horario : horarios) {
             if(horario.getTurma().getCodigoTurma().equals("1DA")){
              if(horario.getDiaSemana()==5){System.out.println(horario);}} // a primeira impressao mosta o sucesso da modificaçao em que o professor foi alterado 
      }
    }

}
        
        



