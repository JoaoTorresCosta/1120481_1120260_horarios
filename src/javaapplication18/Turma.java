/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Pedro
 */
public class Turma {
     private ArrayList<Individuo> alunosTurma= new ArrayList<Individuo>();
     private String codigoTurma; 

    public Turma(String codigoTurma,ArrayList<Individuo> alunosTurma) {
        setCodigoTurma(codigoTurma);
        setAlunosTurma(alunosTurma);
    }

    public ArrayList<Individuo> getAlunosTurma() {
        return alunosTurma;
    }

    private void setAlunosTurma(ArrayList<Individuo> alunosTurma) {
        this.alunosTurma = alunosTurma;
    }

    public String getCodigoTurma() {
        return codigoTurma;
    }

    private void setCodigoTurma(String turma) {
        this.codigoTurma = turma;
    }
  
    @Override
    public String toString() {
        return "Turma{" + "alunosTurma=" + alunosTurma + ", turma=" + codigoTurma + '}';
    }
     
     public static void ListarCrescenteNumero(String Codigo,ArrayList<Turma> turmas){
      ArrayList <Individuo> turmaAordenar=new ArrayList<Individuo>();
        for (Turma turma : turmas) {
            if(turma.getCodigoTurma().equals(Codigo)){
                turmaAordenar=turma.getAlunosTurma();}
        }
        
    Collections.sort(turmaAordenar,new Comparator(){;    
//    Comparator criterio=new Comparator(){
    public int compare(Object o1,Object o2){
        int siglaNumero1=Integer.parseInt(((Individuo)o1).getSiglaOuNumero());
        int siglaNumero2=Integer.parseInt(((Individuo)o2).getSiglaOuNumero());
    if(siglaNumero1==siglaNumero2){return 0;}
    else if(siglaNumero1>siglaNumero2){return 1;}
    else {return -1;}
    }});
        for (int i = 0; i < turmaAordenar.size(); i++) {
            System.out.println(turmaAordenar.get(i));
            
        }
        
   
    }
        public static void ListarDecrescenteNumero(String Codigo,ArrayList<Turma> turmas){
      ArrayList <Individuo> turmaAordenar=new ArrayList<Individuo>();
        for (Turma turma : turmas) {
            if(turma.getCodigoTurma().equals(Codigo)){
                turmaAordenar=turma.getAlunosTurma();}
        }
        
    Collections.sort(turmaAordenar,new Comparator(){;    
//    Comparator criterio=new Comparator(){
    public int compare(Object o1,Object o2){
        int siglaNumero1=Integer.parseInt(((Individuo)o1).getSiglaOuNumero());
        int siglaNumero2=Integer.parseInt(((Individuo)o2).getSiglaOuNumero());
    if(siglaNumero1==siglaNumero2){return 0;}
    else if(siglaNumero1<siglaNumero2){return 1;}
    else {return -1;}
    }});
        for (int i = 0; i < turmaAordenar.size(); i++) {
            System.out.println(turmaAordenar.get(i));
            
        }
        
   
    }
     public static void ListarCrescenteNome(String Codigo,ArrayList<Turma> turmas){
      ArrayList <Individuo> turmaAordenar=new ArrayList<Individuo>();
        for (Turma turma : turmas) {
            if(turma.getCodigoTurma().equals(Codigo)){
                turmaAordenar=turma.getAlunosTurma();}
        }
        
    Collections.sort(turmaAordenar,new Comparator(){;    
//    Comparator criterio=new Comparator(){
    public int compare(Object o1,Object o2){
        String nome1=((Individuo)o1).getNome();
        String nome2=((Individuo)o2).getNome();
    if(nome1.equals(nome2)){return 0;}
    else if(nome1.compareTo(nome2)>0){return 1;}
    else {return -1;}
    }});
        for (int i = 0; i < turmaAordenar.size(); i++) {
            System.out.println(turmaAordenar.get(i));
            
        }
        
   
    }
     public static void ListarDecrescenteNome(String Codigo,ArrayList<Turma> turmas){
      ArrayList <Individuo> turmaAordenar=new ArrayList<Individuo>();
        for (Turma turma : turmas) {
            if(turma.getCodigoTurma().equals(Codigo)){
                turmaAordenar=turma.getAlunosTurma();}
        }
        
    Collections.sort(turmaAordenar,new Comparator(){;    
//    Comparator criterio=new Comparator(){
    public int compare(Object o1,Object o2){
        String nome1=((Individuo)o1).getNome();
        String nome2=((Individuo)o2).getNome();
    if(nome1.equals(nome2)){return 0;}
    else if(nome1.compareTo(nome2)<0){return 1;}
    else {return -1;}
    }});
        for (int i = 0; i < turmaAordenar.size(); i++) {
            System.out.println(turmaAordenar.get(i));
            
        }
        
   
    }
       
        
     public static void listarDadosTurma(String codigo,ArrayList<Turma> turmas){
       Turma turmaAlistar;
       for (Turma turma : turmas) {
           if(turma.getCodigoTurma().equalsIgnoreCase(codigo)){turmaAlistar=(Turma)turma;
           System.out.println(turmaAlistar);}
       }
       
   }
    
    
}

