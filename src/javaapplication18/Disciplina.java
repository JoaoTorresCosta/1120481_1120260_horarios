/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Disciplina {

    
  private String sigla;
  private String discricao;
  private int hSemanaisT;
  private int hSemanaisP;
  private Professor profTeorica;
  private ArrayList<Individuo> profPratica;

    public Disciplina(String sigla, String discricao, int hSemanaisT, int hSemanaisP, Professor profTeorica, ArrayList<Individuo> profPratica) {
       setSigla(sigla);
       setDiscricao(discricao);
       sethSemanaisT(hSemanaisT);
       sethSemanaisP(hSemanaisP);
       setProfTeorica(profTeorica);
       setProfPratica(profPratica);
      }

    public String getSigla() {
        return sigla;
    }

    private void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDiscricao() {
        return discricao;
    }

    private void setDiscricao(String discricao) {
        this.discricao = discricao;
    }

    public int gethSemanaisT() {
        return hSemanaisT;
    }

    private void sethSemanaisT(int hSemanaisT) {
        this.hSemanaisT = hSemanaisT;
    }

    public int gethSemanaisP() {
        return hSemanaisP;
    }

    private void sethSemanaisP(int hSemanaisP) {
        this.hSemanaisP = hSemanaisP;
    }

    public Professor getProfTeorica() {
        return profTeorica;
    }

    private void setProfTeorica(Professor profTeorica) {
        this.profTeorica = profTeorica;
    }

    public ArrayList<Individuo> getProfPratica() {
        return profPratica;
    }

    private void setProfPratica(ArrayList<Individuo> profPratica) {
        this.profPratica = profPratica;
    }

    @Override
    public String toString() {
        return "Disciplina{" + "sigla=" + sigla + ", discricao=" + discricao + ", hSemanaisT=" + hSemanaisT + ", hSemanaisP=" + hSemanaisP + ", profTeorica=" + profTeorica + ", profPratica=" + profPratica + '}';
    }
   
    
   public static void listarDadosDisciplinaSigla(String codigo,ArrayList<Disciplina> disciplinas){
       Disciplina disciplinaAlistar;
       for (Disciplina disciplina : disciplinas) {
           if(disciplina.getSigla().equalsIgnoreCase(codigo)){disciplinaAlistar=(Disciplina)disciplina;
           System.out.println(disciplinaAlistar);
           }
       
       }
       
   }
   public static void listarDadosDisciplinaDescricao(String codigo,ArrayList<Disciplina> disciplinas){
       Disciplina disciplinaAlistar;
       for (Disciplina disciplina : disciplinas) {
           if(disciplina.getDiscricao().equalsIgnoreCase(codigo)){disciplinaAlistar=(Disciplina)disciplina;
           System.out.println(disciplinaAlistar);
           }
         
       }
     
   }
    
   public static void listarDadosDisciplinaProfessorAturma(String nome,ArrayList<Horario>horarios){
      ArrayList<Horario>determinadoProfessor=new ArrayList<Horario>();
       Professor professorAlistar;
       for (Horario horario : horarios) {
           if(horario.getProfessor().getNome().equals(nome)||horario.getProfessor().getSiglaOuNumero().equals(nome)){determinadoProfessor.add(horario);
               
               }
           }
          for (Horario horarioDoProfessor : determinadoProfessor) {
                   System.out.println("Turma:"+horarioDoProfessor.getTurma().getCodigoTurma()+" Disciplinas="+horarioDoProfessor.getSiglaDisciplina().discricao);
         }  
       }
       
   public static void listarDisciplinasNumaSala(ArrayList<Horario>horarios,String sigla){
     ArrayList<Disciplina>disciplinasDeUmaDadaSala=new ArrayList<Disciplina>();
       for (Horario horario : horarios) {
           if(horario.getSala().getCodigo().equalsIgnoreCase(sigla))
           { 
           if(!disciplinasDeUmaDadaSala.contains(horario.getSiglaDisciplina())){
           disciplinasDeUmaDadaSala.add(horario.getSiglaDisciplina());
           }
           }
       }
       System.out.println("As disciplinas da sala "+sigla+" são:");
       for (Disciplina disciplina : disciplinasDeUmaDadaSala) {
           System.out.println(disciplina.getDiscricao());
       }
   }
}
