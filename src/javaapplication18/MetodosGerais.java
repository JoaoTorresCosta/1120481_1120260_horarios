/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import utilitarios.*;
/**
 *
 * @author Pedro
 */
public class MetodosGerais {
    public static Professor procuraProfessor(String nome,ArrayList<Professor>professores){
        for (Professor professor : professores) {
            if(nome.equals(professor.getNome())){
            
            return professor;
            }
            if(nome.equals(professor.getSiglaOuNumero())){
            
            return professor;
            
            }
        }
    return (new Professor(null,null,null,null)); }
    
    public static Object verificaQualObjSeTrata(String codigo,ArrayList<Turma>turmas,ArrayList<Disciplina>disciplinas,ArrayList<Sala>salas,ArrayList<Individuo>alunos,ArrayList<Individuo>professores){
        for (Individuo professor : professores) {
            if(professor.getNome().equalsIgnoreCase(codigo)||professor.getSiglaOuNumero().equalsIgnoreCase(codigo))
            {return professor;}
        }
        for (Sala sala : salas) {
            if(sala.getCodigo().equalsIgnoreCase(codigo))
            {return sala;}
        }
        for (Turma turma : turmas) {
            if(turma.getCodigoTurma().equalsIgnoreCase(codigo))
            {return turma;}
        }
        for (Individuo aluno : alunos) {
            if(aluno.getNome().equalsIgnoreCase(codigo)||aluno.getSiglaOuNumero().equalsIgnoreCase(codigo))
            {return aluno;}
        }
        for (Disciplina disciplina : disciplinas) {
            if(disciplina.getDiscricao().equalsIgnoreCase(codigo)||disciplina.getSigla().equalsIgnoreCase(codigo))
            {return disciplina;}
        }
    
    return (new Object());}
    
    public static void apresentaCargaHoraria(String codigo,ArrayList<Disciplina>disciplinas,ArrayList<Horario>horarios,ArrayList<Turma>turmas,ArrayList<Sala>salas,ArrayList<Individuo>alunos,ArrayList<Individuo>professores){
      int cargaHoraria=0;
      Object objAtratar=verificaQualObjSeTrata(codigo,turmas,disciplinas,salas,alunos,professores);
       
    if(objAtratar==new Object())
    {System.out.println("Foi introduzido um dado invalido");}
    else{
         for (int i = 0; i < horarios.size(); i++) {
            if(horarios.get(i).getProfessor()==objAtratar)
            {
            cargaHoraria+=horarios.get(i).getDuracaoDaAula();
            }
            
            if(horarios.get(i).getSala()==objAtratar)
            {
            cargaHoraria+=horarios.get(i).getDuracaoDaAula();
            }
            
            if(horarios.get(i).getTurma()==objAtratar)
            {
            cargaHoraria+=horarios.get(i).getDuracaoDaAula();
            }
            for (int j = 0; j < horarios.get(i).getTurma().getAlunosTurma().size(); j++) {
            if(horarios.get(i).getTurma().getAlunosTurma().get(j) ==objAtratar)
            {
            cargaHoraria+=horarios.get(i).getDuracaoDaAula();
            }
             }
            if(horarios.get(i).getSiglaDisciplina()==objAtratar)
            {
            cargaHoraria+=horarios.get(i).getDuracaoDaAula();
            }
            
        }
         if(cargaHoraria>0){
         System.out.println("Carga horaria do(a) "+objAtratar+"="+cargaHoraria);}
        }
    
    
     Collections.sort(horarios, new Comparator() {
            ;    
//    Comparator criterio=new Comparator(){
    public int compare(Object o1, Object o2) {
                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
    }
    
 
    
    }

