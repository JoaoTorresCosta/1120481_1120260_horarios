/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Pedro
 */
public class Horario {

    private Turma turma;
    private Disciplina Disciplinaobj;
    private String tipoAula;
    private int diaSemana;
    private int horaInicio;
    private int duracaoDaAula;
    private Professor professor;
    private Sala Sala;

    public Horario(Turma turma, Disciplina sigla, String tipoAula, int diaSemana, int horaInicio, int duracaoDaAula, Professor professor, Sala Sala) {
        setTurma(turma);
        setSiglaDisciplina(sigla);
        setTipoAula(tipoAula); // quando sabemos a sala ja sabemos o tipo de aula
        setDiaSemana(diaSemana);
        setHoraInicio(horaInicio);
        setDuracaoDaAula(duracaoDaAula);
        setProfessor(professor);
        setSala(Sala);
    }

    public Turma getTurma() {
        return turma;
    }

    private void setTurma(Turma turma) {
        this.turma = turma;
    }

    public Disciplina getSiglaDisciplina() {
        return Disciplinaobj;
    }

    private void setSiglaDisciplina(Disciplina sigla) {
        this.Disciplinaobj = sigla;
    }

    public String getTipoAula() {
        return tipoAula;
    }

    private void setTipoAula(String tipoAula) {
        this.tipoAula = tipoAula;
    }

    public int getDiaSemana() {
        return diaSemana;
    }

    private void setDiaSemana(int diaSemana) {
        this.diaSemana = diaSemana;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    private void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getDuracaoDaAula() {
        return duracaoDaAula;
    }

    private void setDuracaoDaAula(int duracaoDaAula) {
        this.duracaoDaAula = duracaoDaAula;
    }

    public Professor getProfessor() {
        return professor;
    }

    private void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Sala getSala() {
        return Sala;
    }

    private void setSala(Sala Sala) {
        this.Sala = Sala;
    }

    @Override
    public String toString() {
        return "Horario{" + "turma=" + turma.getCodigoTurma() + ", sigla=" + Disciplinaobj.getDiscricao() + ", tipoAula=" + tipoAula + ", diaSemana=" + diaSemana + ", horaInicio=" + horaInicio + ", duracaoDaAula=" + duracaoDaAula + ", professor=" + professor.getSiglaOuNumero() + ", Sala=" + Sala.getCodigo() + '}';
    }

    public static void encontrarPeriodoLivre(int duracaoEvento, int diaDaSemana, int horaApartirDaQualPretendeMarcar, String sala, ArrayList<Horario> horarios) {


        Collections.sort(horarios, new Comparator() {
            public int compare(Object o1, Object o2) {
                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });



        ArrayList<Horario> horarioauxiliar = new ArrayList<Horario>();
        horaApartirDaQualPretendeMarcar = 8;
        for (Horario horario : horarios) {
            if (horario.getDiaSemana() == diaDaSemana) {
                if (horario.getSala().getCodigo().equals(sala)) {
                    horarioauxiliar.add(horario.horaInicio, horario);
                    if (horario.getDuracaoDaAula() == 2) {
                        horarioauxiliar.add(horario.horaInicio, horario);
                    }

                } else if (!horario.getSala().getCodigo().equals(sala)) {
                    horarioauxiliar.add(null);
                }

            }

        }

        int count = 0;
        for (int i = horaApartirDaQualPretendeMarcar; i < 18; i++) {
            if (horarioauxiliar.get(i) == null) {
                for (int j = i; j < 18; j++) {
                    if (horarioauxiliar.get(j) == null) {
                        if ((j - i) == duracaoEvento) {
                            count++;
                            if (count == 1) {
                                System.out.println("A partir da hora " + (horaApartirDaQualPretendeMarcar) + " tem livre a sala " + sala + " no dia " + diaDaSemana + " nas seguintes horas: ");
                            }
                            System.out.println((i) + " até " + (j));
                        }
                    }

                }

            }
        }


        if (count == 0) {
            System.out.println("A sala " + sala + " no dia " + diaDaSemana + " nao se encontra disponivel para um evento de " + duracaoEvento + " horas");
        }

    }

    public static void apresentarHorarioIndividuo(String Nomeindividuo, ArrayList<Horario> horarios) {
        ArrayList<Horario> horariosAuxiliar = new ArrayList<Horario>();
        ArrayList<Horario> horarioSegunda = new ArrayList<Horario>();
        ArrayList<Horario> horarioTerca = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuarta = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuinta = new ArrayList<Horario>();
        ArrayList<Horario> horarioSexta = new ArrayList<Horario>();


        Collections.sort(horarios, new Comparator() {
            public int compare(Object o1, Object o2) {

                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        for (Horario horario : horarios) {
            if (horario.professor.getNome().equalsIgnoreCase(Nomeindividuo) || horario.professor.getSiglaOuNumero().equalsIgnoreCase(Nomeindividuo)) {
                horariosAuxiliar.add(horario);
            } else {
                for (int i = 0; i < horario.turma.getAlunosTurma().size(); i++) {
                    if (horario.turma.getAlunosTurma().get(i).getNome().equalsIgnoreCase(Nomeindividuo) || horario.turma.getAlunosTurma().get(i).getSiglaOuNumero().equalsIgnoreCase(Nomeindividuo)) {
                        horariosAuxiliar.add(horario);
                    }

                }

            }


        }

        for (Horario horario : horariosAuxiliar) {

            if (horario.diaSemana == 2) {
                horarioSegunda.add(horario);
            }
            if (horario.diaSemana == 3) {
                horarioTerca.add(horario);
            }
            if (horario.diaSemana == 4) {
                horarioQuarta.add(horario);
            }
            if (horario.diaSemana == 5) {
                horarioQuinta.add(horario);
            }
            if (horario.diaSemana == 6) {
                horarioSexta.add(horario);
            }

        }
        System.out.println("Segunta feira");
        for (Horario horario : horarioSegunda) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Terça feira");
        for (Horario horario : horarioTerca) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quarta feira");
        for (Horario horario : horarioQuarta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quinta feira");
        for (Horario horario : horarioQuinta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Sexta feira");
        for (Horario horario : horarioSexta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }

    }

    public static void mostarHorariosSala(String codigo, ArrayList<Horario> horarios) {
        ArrayList<Horario> horariosAuxiliar = new ArrayList<Horario>();
        ArrayList<Horario> horarioSegunda = new ArrayList<Horario>();
        ArrayList<Horario> horarioTerca = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuarta = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuinta = new ArrayList<Horario>();
        ArrayList<Horario> horarioSexta = new ArrayList<Horario>();


        Collections.sort(horarios, new Comparator() {
            public int compare(Object o1, Object o2) {

                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        for (Horario horario : horarios) {
            if (horario.Sala.getCodigo().equalsIgnoreCase(codigo)) {
                horariosAuxiliar.add(horario);
            }
        }

        for (Horario horario : horariosAuxiliar) {

            if (horario.diaSemana == 2) {
                horarioSegunda.add(horario);
            }
            if (horario.diaSemana == 3) {
                horarioTerca.add(horario);
            }
            if (horario.diaSemana == 4) {
                horarioQuarta.add(horario);
            }
            if (horario.diaSemana == 5) {
                horarioQuinta.add(horario);
            }
            if (horario.diaSemana == 6) {
                horarioSexta.add(horario);
            }

        }
        System.out.println("Segunta feira");
        for (Horario horario : horarioSegunda) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Turma: " + horario.turma.getCodigoTurma() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Terça feira");
        for (Horario horario : horarioTerca) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Turma: " + horario.turma.getCodigoTurma() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quarta feira");
        for (Horario horario : horarioQuarta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Turma: " + horario.turma.getCodigoTurma() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quinta feira");
        for (Horario horario : horarioQuinta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Turma: " + horario.turma.getCodigoTurma() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Sexta feira");
        for (Horario horario : horarioSexta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Turma: " + horario.turma.getCodigoTurma() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }

    }

    public static void mostarHorariosTurma(String turmaAmostrar, ArrayList<Horario> horarios) {
        ArrayList<Horario> horariosAuxiliar = new ArrayList<Horario>();
        ArrayList<Horario> horarioSegunda = new ArrayList<Horario>();
        ArrayList<Horario> horarioTerca = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuarta = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuinta = new ArrayList<Horario>();
        ArrayList<Horario> horarioSexta = new ArrayList<Horario>();


        Collections.sort(horarios, new Comparator() {
            public int compare(Object o1, Object o2) {

                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        for (Horario horario : horarios) {
            if (horario.turma.getCodigoTurma().equalsIgnoreCase(turmaAmostrar)) {
                horariosAuxiliar.add(horario);
            }
        }

        for (Horario horario : horariosAuxiliar) {

            if (horario.diaSemana == 2) {
                horarioSegunda.add(horario);
            }
            if (horario.diaSemana == 3) {
                horarioTerca.add(horario);
            }
            if (horario.diaSemana == 4) {
                horarioQuarta.add(horario);
            }
            if (horario.diaSemana == 5) {
                horarioQuinta.add(horario);
            }
            if (horario.diaSemana == 6) {
                horarioSexta.add(horario);
            }

        }
        System.out.println("Segunta feira");
        for (Horario horario : horarioSegunda) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Terça feira");
        for (Horario horario : horarioTerca) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quarta feira");
        for (Horario horario : horarioQuarta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quinta feira");
        for (Horario horario : horarioQuinta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Sexta feira");
        for (Horario horario : horarioSexta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }

    }

    public static void mostarHorariosDisciplina(String disciplinaAmostrar, ArrayList<Horario> horarios) {
        ArrayList<Horario> horariosAuxiliar = new ArrayList<Horario>();
        ArrayList<Horario> horarioSegunda = new ArrayList<Horario>();
        ArrayList<Horario> horarioTerca = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuarta = new ArrayList<Horario>();
        ArrayList<Horario> horarioQuinta = new ArrayList<Horario>();
        ArrayList<Horario> horarioSexta = new ArrayList<Horario>();


        Collections.sort(horarios, new Comparator() {
            public int compare(Object o1, Object o2) {

                int horaInicio1 = ((Horario) o1).getHoraInicio();
                int horaInicio2 = ((Horario) o2).getHoraInicio();
                if (horaInicio1 == horaInicio2) {
                    return 0;
                } else if (horaInicio2 < horaInicio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        for (Horario horario : horarios) {
            if (horario.getSiglaDisciplina().getSigla().equalsIgnoreCase(disciplinaAmostrar) || horario.getSiglaDisciplina().getDiscricao().equalsIgnoreCase(disciplinaAmostrar)) {
                horariosAuxiliar.add(horario);
            }
        }

        for (Horario horario : horariosAuxiliar) {

            if (horario.diaSemana == 2) {
                horarioSegunda.add(horario);
            }
            if (horario.diaSemana == 3) {
                horarioTerca.add(horario);
            }
            if (horario.diaSemana == 4) {
                horarioQuarta.add(horario);
            }
            if (horario.diaSemana == 5) {
                horarioQuinta.add(horario);
            }
            if (horario.diaSemana == 6) {
                horarioSexta.add(horario);
            }

        }
        System.out.println("Segunta feira");
        for (Horario horario : horarioSegunda) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Terça feira");
        for (Horario horario : horarioTerca) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quarta feira");
        for (Horario horario : horarioQuarta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Quinta feira");
        for (Horario horario : horarioQuinta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }
        System.out.println("Sexta feira");
        for (Horario horario : horarioSexta) {
            System.out.println(horario.getHoraInicio() + " até " + (horario.getDuracaoDaAula() + horario.getHoraInicio()) + " Na sala: " + horario.getSala().getCodigo() + " Disciplina:" + horario.getSiglaDisciplina().getDiscricao());
        }





    }

    public static ArrayList<Horario> InserirCelula(Horario celulaHorario, ArrayList<Horario> horarios) {
        int SaberSeAsalaTaEmUso = 0;
        ArrayList<Horario> horarioaux = new ArrayList<Horario>();
        boolean taLivre = true;
        for (int i = 0; i < horarios.size(); i++) {
            if (horarios.get(i).getDiaSemana() == celulaHorario.diaSemana) {
                if (horarios.get(i).getHoraInicio() == celulaHorario.horaInicio) {
                    horarioaux.add(horarios.get(i));
                }
            }
        }
        for (int i = 0; i < horarios.size(); i++) {
            if (horarios.get(i).getDiaSemana() == celulaHorario.diaSemana) {
                if (horarios.get(i).getHoraInicio() != celulaHorario.horaInicio) {
                    if ((celulaHorario.horaInicio + celulaHorario.duracaoDaAula) != horarios.get(i).horaInicio) {
                        for (int j = 0; j < horarios.size(); j++) {
                            if (horarios.get(j).getDiaSemana() == celulaHorario.diaSemana) {
                                if (horarios.get(j).getHoraInicio() == celulaHorario.horaInicio) {
                                    if (horarios.get(j).Sala.equals(celulaHorario.Sala)) {
                                        SaberSeAsalaTaEmUso++;
                                    }
                                }
                            }

                        }
                        if (SaberSeAsalaTaEmUso == 0) {
                            for (int j = 0; j < horarioaux.size(); j++) {
                                if (horarioaux.get(i).professor.equals(celulaHorario.professor)) {
                                    taLivre = false;
                                }
                                if (taLivre == true) {
                                    horarios.add(celulaHorario);
                                    return horarios;
                                }
                            }
                        }
                    }
                }
            }
        }
        return horarios;
    }

    public static ArrayList<Horario> EditarCelula(int diaSemana, int horaInicio, String nomeTurma, Sala salaNova, Individuo professorNovo, Disciplina disciplinaNova, ArrayList<Horario> horarios) {
        boolean taLivre = true;
        int duracaoAulaNova;
        String tipoAulaNova;
        int SaberSeAsalaTaEmUso = 0;
        ArrayList<Horario> horarioaux = new ArrayList<Horario>();

        for (int i = 0; i < horarios.size(); i++) {
            if (horarios.get(i).getDiaSemana() == diaSemana) {
                if (horarios.get(i).getHoraInicio() == horaInicio) {
                    horarioaux.add(horarios.get(i));
                }
            }
        }


        for (int i = 0; i < horarios.size(); i++) {
            if (horarios.get(i).getDiaSemana() == diaSemana) {
                if (horarios.get(i).getHoraInicio() == horaInicio) {
                    if (horarios.get(i).getTurma().getCodigoTurma().equalsIgnoreCase(nomeTurma)) {

                        if (salaNova != null) {
                            if (salaNova.getTipoSala().equalsIgnoreCase(horarios.get(i).Sala.getTipoSala())) {

                                for (int j = 0; j < horarioaux.size(); j++) {
                                    if (horarioaux.get(j).Sala.getCodigo().equals(salaNova.getCodigo())) {
                                        SaberSeAsalaTaEmUso++;
                                    }

                                }



                                if (SaberSeAsalaTaEmUso == 0 || horarios.get(i).Sala.getCodigo().equalsIgnoreCase(salaNova.getCodigo())) {
                                    if (disciplinaNova != null) {
                                        if (professorNovo != null) {
                                            for (int k = 0; k < horarioaux.size(); k++) {
                                                if (horarioaux.get(k).professor.equals(professorNovo)) {
                                                    taLivre = false;
                                                }
                                            }
                                            if (taLivre == true) {
                                                if (horarios.get(i).tipoAula.equalsIgnoreCase("teorica")) {
                                                    if (disciplinaNova.getProfTeorica().equals(professorNovo)) {

                                                        horarios.get(i).setProfessor((Professor) professorNovo);
                                                        horarios.get(i).setSala(salaNova);
                                                        horarios.get(i).setSiglaDisciplina(disciplinaNova);
                                                        System.out.println("Alteração com sucesso.");
                                                        return horarios;
                                                    }
                                                } else {
                                                    if (disciplinaNova.getProfPratica().contains(professorNovo)) {

                                                        horarios.get(i).setProfessor((Professor) professorNovo);
                                                        horarios.get(i).setSala(salaNova);
                                                        horarios.get(i).setSiglaDisciplina(disciplinaNova);
                                                        System.out.println("Alteração com sucesso.");
                                                        return horarios;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
            }
        }
        System.out.println("Nao houve exito algum dado esta incorreto, algum professor ou sala estao a ser utuiizados");
        return horarios;
    }

    public static ArrayList<Horario> EliminarCelula(int diaSemana, int horaInicio, String nomeTurma, ArrayList<Horario> horarios) {
        for (Horario horario : horarios) {
            if (horario.getDiaSemana() == diaSemana) {
                if (horario.getHoraInicio() == horaInicio) {
                    if (horario.getTurma().getCodigoTurma().equalsIgnoreCase(nomeTurma)) {
                        try {
                            horarios.remove(horario);
                            return horarios;
                        } catch (Exception e) {
                            System.out.println("Não foi possivel eliminar.");
                        }
                    }
                }
            }
        }
        return horarios;
    }
}
            

