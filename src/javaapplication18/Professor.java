/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication18;
import utilitarios.*;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Professor extends Individuo {
    
    public Professor(String sigla, String nome, Data data,String mail) {
        super(sigla,nome,mail,data);
        
    }

    @Override
    public String toString() {
        return "Professor{" + "sigla=" + getSiglaOuNumero() +" nome="+getNome() +" Data de contrataçao="+getData()+" mail="+getMail()+'}';
    }

 public static void listarDadosProfessorNome(String nomeProfessor,ArrayList<Individuo> professores){
       Professor professorAlistar;
       for (Individuo professor : professores) {
           if(professor.getNome().equalsIgnoreCase(nomeProfessor)){professorAlistar=(Professor)professor;
            System.out.println(professorAlistar);
           }
       }
      
   }
 public static void listarDadosProfessorSigla(String siglaProfessor,ArrayList<Individuo> professores){
       Professor professorAlistar;
       for (Individuo professor : professores) {
           if(professor.getSiglaOuNumero().equalsIgnoreCase(siglaProfessor)){professorAlistar=(Professor)professor;
           System.out.println(professorAlistar);
           }
       }
       
   }

public static void listarProfessoresDeUmaDadaTurma(ArrayList<Horario>horarios,String codigoTurma){
    for (Horario horario : horarios) {
        if(horario.getTurma().getCodigoTurma().equalsIgnoreCase(codigoTurma))
        {
            System.out.println("O professor "+horario.getProfessor().getNome()+" leciona a componente "+horario.getTipoAula()+" da disciplina"+horario.getSiglaDisciplina().getDiscricao()+" à turma "+horario.getTurma().getCodigoTurma());
        }
    }

}



}


