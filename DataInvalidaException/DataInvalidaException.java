﻿public class DataInvalidaException extends Exception {

    public DataInvalidaException() {
        super("Data inválida!!!");
    }

    public DataInvalidaException(String msg) {
        super(msg);
    }
}
