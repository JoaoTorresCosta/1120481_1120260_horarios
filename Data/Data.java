﻿public class Data {

    private static enum DiaDaSemana {

        // -- valores do tipo -----------------------------------------------------------------
        DOMINGO { public String toString() { return "Domingo"; } },
        SEGUNDA { public String toString() { return "Segunda-Feira"; } },
        TERCA { public String toString() { return "Terça-Feira"; } },
        QUARTA { public String toString() { return "Quarta-Feira"; } },
        QUINTA { public String toString() { return "Quinta-Feira"; } },
        SEXTA { public String toString() { return "Sexta-Feira"; } },
        SABADO { public String toString() { return "Sábado"; } };

        // -- metodo de classe ----------------------------------------------------------------
        public static String designacaoDiaDaSemana(int ordemDiaDaSemana) {
            return DiaDaSemana.values()[ordemDiaDaSemana].toString();
        }
    }

    private static enum Mes {

        // -- valores do tipo -----------------------------------------------------------------
        Janeiro(31), Fevereiro(28), Marco(31) { public String toString() { return "Março"; } },
        Abril(30), Maio(31), Junho(30), Julho(31), Agosto(31), Setembro(30),
        Outubro(31), Novembro(30), Dezembro(31);
        
        // -- variavel de instancia -----------------------------------------------------------
        private int totalDeDias;

        // -- construtores --------------------------------------------------------------------
        private Mes(int totalDeDias) {
            this.totalDeDias = totalDeDias;
        }

        // -- metodo de classe ---------------------------------------------------------------
        public static Mes obterMes(int ordemDoMes) {
            return ordemDoMes>0 && ordemDoMes<=12 ? Mes.values()[ordemDoMes - 1] : null;
        }
        
        // -- metodo de instancia -------------------------------------------------------------
        public int numeroDeDias(int ano) {
            if (ordinal() == 1 && isAnoBissexto(ano)) {
                return ++totalDeDias;
            }
            return totalDeDias;
        }
        
        // -- metodo auxiliar -------------------------------------------------------------
        private static boolean isAnoBissexto(int ano) {
            return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
        }
    }
    
    // -- variaveis de instancia --------------------------------------------------------------
    private int ano;
    private Mes mes;
    private int dia;

    // -- construtores ------------------------------------------------------------------------
    public Data() {
        ano = 1;
        mes = Mes.Janeiro;
        dia = 1;
    }

    public Data(int ano, int mes, int dia) throws DataInvalidaException {
        setData(ano, mes, dia);
    }
    
    public Data(Data d){
        ano = d.ano;
        mes = d.mes;
        dia = d.dia;
    }

    // -- metodos de classe -------------------------------------------------------------------
    public static boolean isAnoBissexto(int ano) {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }
    
    // -- metodos de instancia ----------------------------------------------------------------
    
    // ----- metodos de consulta
    public int getAno() {
        return ano;
    }

    public int getMes() {
        return mes.ordinal() + 1;
    }

    public int getDia() {
        return dia;
    }

    // ----- metodo de modificacao
    public void setData(int ano, int mes, int dia) throws DataInvalidaException {
        if (mes > 0 && mes <= 12) {
            Mes m = Mes.obterMes(mes);
            if (dia > 0 && dia <= m.numeroDeDias(ano)) {
                this.ano = ano;
                this.mes = m;
                this.dia = dia;
            } else {
                throw new DataInvalidaException("Dia Inválido!!");
            }
        } else {
            throw new DataInvalidaException("Mês Inválido!!");
        }
    }

    // ----- metodos complementares
    public String toAnoMesDiaString() {
        return String.format("%04d/%02d/%02d", ano, mes, dia);
    }

    public String toString() {
        return diaDaSemana() + ", " + dia + " de " + mes + " de " + ano;
    }

    public String diaDaSemana() {
        int totalDias = contaDias();

        totalDias = totalDias % 7;

        return DiaDaSemana.designacaoDiaDaSemana(totalDias);
    }

    public boolean maior(Data d1) {
        int totalDias = contaDias();
        int totalDias1 = d1.contaDias();

        return totalDias > totalDias1;
    }

    public int diferenca(Data d1) {
        int totalDias = contaDias();
        int totalDias1 = d1.contaDias();

        return Math.abs(totalDias - totalDias1);
    }

    // ----- metodo auxiliar
    private int contaDias() {
        int totalDias = 0;

        for (int i = 1; i < ano; i++) {
            totalDias += isAnoBissexto(i) ? 366 : 365;
        }
        for (int i = 1; i < mes.ordinal() + 1; i++) {
            totalDias += Mes.obterMes(i).numeroDeDias(ano);
        }
        totalDias += dia;

        return totalDias;
    }
    
}

