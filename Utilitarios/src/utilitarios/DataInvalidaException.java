/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

/**
 *
 * @author Pedro
 */
public class DataInvalidaException extends Exception {

    public DataInvalidaException() {
        super("Data inválida!!!");
    }

    public DataInvalidaException(String msg) {
        super(msg);
    }
}